import React,{Component} from 'react'

class App extends Component {
    state = {
      view:"Sign In",
      presence:"hidden",
      secondview:"visible",
      text:"Don't have an account? Sign up",
      action:'/login'
    };


     signup = () =>{
       this.setState({view:"Sign Up"});
       this.setState({presence:"visible"});
       this.setState({secondview:"hidden"});
       this.setState({action:"/signup"});
     }
     
 render() {     
     return(
       <div style={{textAlign:"center",marginTop:"10%"}}>
       <h1 style={{color:"#33b5e5",fontFamily:"Adventure Subtitles"}}>HobbyList</h1>
     <h4 style={{color:"#33b5e5",fontFamily:"Adventure Subtitles"}}>All your hobbies in one place</h4>
     <form  style={{width:"50%",position:"absolute",left:"25%"}} method="POST"
     action={this.state.action}>
         <input type="text" className="form-control m-2" name="firstname" 
         style={{visibility:this.state.presence}} placeholder="First Name"/>
         <input type="text" className="form-control m-2" name="lastname"
          style={{visibility:this.state.presence}} placeholder="Last Name"/>
         <input type="email" placeholder="Email" name="email" className="form-control m-2"/>
         <input type="text"  className="form-control m-2" name="phonenumber"
          style={{visibility:this.state.presence}} placeholder="Phone Number"/>
         <input type="password" className="form-control m-2" name="password"
         placeholder="Password"/>
         <button className="btn btn-primary btn-block m-2"  type="submit">{this.state.view}</button>
      <a href="#" onClick={this.signup} style={{visibility:this.state.secondview}}>{this.state.text}</a>
     </form>
  </div> 
      );
 }

}

export default App;

